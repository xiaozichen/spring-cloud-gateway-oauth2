package com.sakura.demo.auth.jwt;

import com.sakura.demo.common.model.SecurityUser;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: zhengcan
 * @Date: 2022/5/12
 * @Description: JwtToken增强器
 * @Version: 1.0.0 创建
 */
public class JwtTokenEnhancer implements TokenEnhancer {

    /**
     * 可将用户自定义的信息放进jwt token中
     * @param oAuth2AccessToken
     * @param oAuth2Authentication
     * @return
     */
    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken oAuth2AccessToken, OAuth2Authentication oAuth2Authentication) {
        SecurityUser user = (SecurityUser) oAuth2Authentication.getPrincipal();
        Map<String, Object> info = new HashMap<>();
        // 例如将用户id放入jwt token中
        info.put("id", user.getId());
        info.put("test", "this is test additional info");
        ((DefaultOAuth2AccessToken)oAuth2AccessToken).setAdditionalInformation(info);
        return oAuth2AccessToken;
    }
}

package com.sakura.demo.auth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

import javax.sql.DataSource;

/**
 * @Author: zhengcan
 * @Date: 2022/5/17
 * @Description: jwt token存储方式配置
 * @Version: 1.0.0 创建
 */
@Configuration
public class JwtTokenStoreConfig {

    @Autowired
    private DataSource dataSource;

    /**
     * jdbc存储token模式
     * @return
     */
    @Bean
    public TokenStore sakuraTokenStore() {
        return new JdbcTokenStore(dataSource);
    }

//    @Autowired
//    private RedisConnectionFactory redisConnectionFactory;
//
//    /**
//     * redis存储token模式
//     * @return
//     */
//    @Bean
//    public TokenStore sakuraTokenStore() {
//        return new RedisTokenStore(redisConnectionFactory);
//    }

//    @Bean
//    public TokenStore sakuraTokenStore() {
//        return new InMemoryTokenStore();
//    }
}

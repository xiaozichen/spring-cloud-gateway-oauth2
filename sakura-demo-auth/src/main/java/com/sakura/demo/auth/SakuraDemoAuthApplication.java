package com.sakura.demo.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: Nelsoncan
 * @Date: 2022/5/12
 * @Description:
 * @Version: 1.0.0 创建
 */
@SpringBootApplication
public class SakuraDemoAuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(SakuraDemoAuthApplication.class, args);
    }
}

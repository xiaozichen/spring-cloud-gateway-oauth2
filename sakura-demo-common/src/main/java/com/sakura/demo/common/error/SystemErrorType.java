package com.sakura.demo.common.error;

import com.sakura.demo.common.constant.MessageConstant;
import lombok.Getter;

/**
 * @Author: zhengcan
 * @Date: 2022/3/21
 * @Description: 系统异常描述
 * @Version: 1.0.0 创建
 */
@Getter
public enum SystemErrorType implements ErrorType {

    SYSTEM_ERROR(500, "系统异常"),
    VALIDATE_FAILED(404, "参数检验失败"),
    UNAUTHORIZED(401, MessageConstant.INVALID_TOKEN),
    FORBIDDEN(403, MessageConstant.PERMISSION_DENIED);


    /**
     * 错误类型码
     */
    private Integer code;
    /**
     * 错误类型描述信息
     */
    private String message;

    SystemErrorType(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}

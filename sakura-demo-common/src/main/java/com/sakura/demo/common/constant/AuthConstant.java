package com.sakura.demo.common.constant;

/**
 * @Author: zhengcan
 * @Date: 2022/5/13
 * @Description:
 * @Version: 1.0.0 创建
 */
public class AuthConstant {

    public static final String TOKEN_HEADER = "Authorization";

    public static final String AUTHORITY_PREFIX = "ROLE_";

    public static final String AUTHORITY_CLAIM_NAME = "authorities";

    public static final String TOKEN_STANDARD_STYLE = "Bearer ";

    public static final String USER_INFO_HEADER = "UserInfo";

}

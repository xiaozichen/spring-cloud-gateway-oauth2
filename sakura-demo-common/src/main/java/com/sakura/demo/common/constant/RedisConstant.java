package com.sakura.demo.common.constant;

/**
 * @Author: zhengcan
 * @Date: 2022/5/12
 * @Description:
 * @Version: 1.0.0 创建
 */
public class RedisConstant {

    public static final String RESOURCE_ROLES_MAP = "AUTH:RESOURCE_ROLES_MAP";

}

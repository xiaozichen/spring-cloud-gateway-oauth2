package com.sakura.demo.common.error;

/**
 * @Author: zhengcan
 * @Date: 2022/3/21
 * @Description: 异常类型
 * @Version: 1.0.0 创建
 */
public interface ErrorType {
    /**
     * 返回code
     *
     * @return
     */
    Integer getCode();

    /**
     * 返回mesg
     *
     * @return
     */
    String getMessage();
}

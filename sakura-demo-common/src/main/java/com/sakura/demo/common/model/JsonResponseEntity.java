package com.sakura.demo.common.model;

import com.sakura.demo.common.utils.FastJsonUtil;
import org.springframework.http.HttpStatus;
import org.springframework.util.Assert;

import java.io.Serializable;

/**
 * @Author: zhengcan
 * @Date: 2022/5/11
 * @Description: JSON格式的响应体
 * @Version: 1.0.0 创建
 */
public class JsonResponseEntity<T> implements Serializable {
    private static final long serialVersionUID = 8422478323030221531L;
    private String message;
    private int code;
    private T data;

    public JsonResponseEntity() {
        this.message = HttpStatus.OK.getReasonPhrase();
        this.code = HttpStatus.OK.value();
    }

    public JsonResponseEntity(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public JsonResponseEntity(String message, int code, T data) {
        this.message = message;
        this.code = code;
        this.data = data;
    }

    public static <T> JsonResponseEntity<T> builder() {
        return new JsonResponseEntity();
    }

    public static <T> JsonResponseEntity<T> buildOK(T data) {
        return (JsonResponseEntity<T>) builder().setResponse(data);
    }

    public static <T> JsonResponseEntity<T> buildOK(String message, T data) {
        return buildOK(data).setMessage(message);
    }

    public static JsonResponseEntity buildSysTemError() {
        return builder().setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public static JsonResponseEntity buildSysTemError(String message) {
        return buildSysTemError().setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR).setMessage(message);
    }

    public static <T> JsonResponseEntity<T> buildBusinessError() {
        return builder().setHttpStatus(HttpStatus.BAD_REQUEST);
    }

    public static JsonResponseEntity buildBusinessError(String message) {
        return buildBusinessError().setMessage(message);
    }

    public static JsonResponseEntity buildBusinessError(String message, int status) {
        return buildBusinessError().setMessage(message).setCode(status);
    }

    public JsonResponseEntity setMessage(String message) {
        this.message = message;
        return this;
    }

    public JsonResponseEntity setCode(int code) {
        this.code = code;
        return this;
    }

    public JsonResponseEntity<T> setResponse(T data) {
        this.data = data;
        return this;
    }

    public JsonResponseEntity setHttpStatus(HttpStatus httpStatus) {
        Assert.notNull(httpStatus, "httpStatus can not be null");
        this.setMessage(httpStatus.getReasonPhrase());
        this.setCode(httpStatus.value());
        return this;
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isSuccessful() {
        return HttpStatus.OK.value() == this.code;
    }

    public boolean isFailure() {
        return !this.isSuccessful();
    }

    @Override
    public String toString() {
        return FastJsonUtil.to(this);
    }
}

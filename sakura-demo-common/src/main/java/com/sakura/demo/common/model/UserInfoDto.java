package com.sakura.demo.common.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @Author: zhengcan
 * @Date: 2022/5/17
 * @Description: 用于请求头存放用户信息
 * @Version: 1.0.0 创建
 */
@Data
public class UserInfoDto {

    /**
     * 用户ID
     */
    private Long id;

    /**
     * 用户名
     */
    @JsonProperty("user_name")
    private String username;

    /**
     * 客户端ID
     */
    @JsonProperty("client_id")
    private String clientId;

    /**
     * 作用域
     */
    private String scope;

    /**
     * 过期时间
     */
    private Long exp;

    /**
     * 用户权限
     */
    private List<String> authorities;

    /**
     * jwt标识
     */
    private String jti;

}

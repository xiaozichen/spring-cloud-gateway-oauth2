package com.sakura.demo.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @Author: Nelsoncan
 * @Date: 2022/5/12
 * @Description:
 * @Version: 1.0.0 创建
 */
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
public class UserDto {
    /**
     * 用户ID
     */
    private Long id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 用户密码
     */
    private String password;
    /**
     * 用户状态
     */
    private Integer status;
    /**
     * 用户角色
     */
    private List<String> roles;
}

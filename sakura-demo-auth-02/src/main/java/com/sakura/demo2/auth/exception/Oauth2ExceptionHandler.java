package com.sakura.demo2.auth.exception;

import com.sakura.demo.common.model.JsonResponseEntity;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author: zhengcan
 * @Date: 2022/5/12
 * @Description: 全局处理Oauth2抛出的异常
 * @Version: 1.0.0 创建
 */
@ControllerAdvice
public class Oauth2ExceptionHandler {

    @ResponseBody
    @ExceptionHandler(value = OAuth2Exception.class)
    public JsonResponseEntity handleOauth2(OAuth2Exception e) {
        return JsonResponseEntity.buildSysTemError(e.getMessage());
    }

}

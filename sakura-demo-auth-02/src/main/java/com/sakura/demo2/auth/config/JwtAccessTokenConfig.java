package com.sakura.demo2.auth.config;

import com.sakura.demo2.auth.jwt.JwtTokenEnhancer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

/**
 * @Author: zhengcan
 * @Date: 2022/5/17
 * @Description: jwt access token配置
 * @Version: 1.0.0 创建
 */
@Configuration
public class JwtAccessTokenConfig {

    @Value("${jwt.secret}")
    private String jwtSecret;

    /**
     * token转换器
     * @return
     */
    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
        // 对称加密模式
        jwtAccessTokenConverter.setSigningKey(jwtSecret);
        return jwtAccessTokenConverter;
    }

    /**
     * token增强器
     * @return
     */
    @Bean
    public JwtTokenEnhancer jwtTokenEnhancer() {
        return new JwtTokenEnhancer();
    }

}

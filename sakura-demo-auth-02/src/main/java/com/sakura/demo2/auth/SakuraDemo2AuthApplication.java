package com.sakura.demo2.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: Nelsoncan
 * @Date: 2022/5/12
 * @Description:
 * @Version: 1.0.0 创建
 */
@SpringBootApplication
public class SakuraDemo2AuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(SakuraDemo2AuthApplication.class, args);
    }
}

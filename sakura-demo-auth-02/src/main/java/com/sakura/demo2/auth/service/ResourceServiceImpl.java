package com.sakura.demo2.auth.service;

import cn.hutool.core.collection.CollUtil;
import com.sakura.demo.common.constant.RedisConstant;
import com.sakura.demo.common.redis.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @Author: zhengcan
 * @Date: 2022/5/12
 * @Description: 资源与角色匹配关系管理业务类
 * @Version: 1.0.0 创建
 */
@Service
public class ResourceServiceImpl {

    private Map<String, List<String>> resourceRolesMap;

    @Autowired
    private RedisService redisService;

    /**
     * 模拟业务，初始化一下数据
     */
    @PostConstruct
    public void initData() {
        resourceRolesMap = new TreeMap<>();
        resourceRolesMap.put("/sakura-demo-admin/user/hello", CollUtil.toList("ADMIN"));
        resourceRolesMap.put("/sakura-demo-admin/user/currentUser", CollUtil.toList("ADMIN", "TEST"));
        resourceRolesMap.put("/sakura-demo-auth/user/logout", CollUtil.toList("ADMIN", "TEST"));
        resourceRolesMap.put("/sakura-demo-auth-02/user/logout", CollUtil.toList("ADMIN", "TEST"));
        redisService.hSetAll(RedisConstant.RESOURCE_ROLES_MAP, resourceRolesMap);
    }

}

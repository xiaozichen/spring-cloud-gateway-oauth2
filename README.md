# spring-cloud-gateway-oauth2

#### 介绍
SpringCloud Gateway网关和Oauth2整合的项目

#### 软件架构
 软件架构说明
1. 原本想做个SpringCloud Gateway与Oauth2整合的项目，将认证和鉴权放在网关层统一处理，参考了一些网友的案例，
但是在实际的开发过程中遇到各种各样的问题，比如jar包选取问题，jwt token加解密或签名问题，认证通过后鉴权信息丢失无法鉴权问题
，还有就是不同的token存储方式导致的token认证问题，等等不一而足。
2. 这些实际中的问题无法在网友的demo案例中发现或解决，如是花了一些时间调试，争取写一个适当的案例。
3. 实际开发过程中可以自己自定义过滤器处理我们需要解决的问题，以达到同样的目的。
#### 安装教程

1.  直接下载源码编译
2.  MySQL，Redis，Nacos等中间件是需要的
3.  jwt密钥文件的生成可以参考网上的生成方式

#### 使用说明

1. 由admin,auth,gateway-01三个服务组成一种实现方式，主要功能包括：jdbc,redis,memory三种token
    存储方式的实现；支持密码模式，授权码模式，刷新token模式的授权认证管理；jwt token采用非对称加密的方式；网关层token验证由ResourceServer调认证服务器的公钥
    接口获取公钥解析token，然后由JwtReactiveAuthenticationManager管理器来认证；网关层权限验证由自定义
    的ReactiveAuthorizationManager授权管理器实现动态鉴权。
2. 由admin,auth-02,gateway-02三个服务组成另一种种实现方式，主要功能包括：jdbc,redis,memory三种token
   存储方式的实现；支持密码模式，授权码模式，刷新token模式的授权认证管理；jwt token采用对称加密的方式；网关层token验证由自定义的token管理器处理验证，解密方式和认证服务器加密方式保持一致；网关层权限验证由自定义
   的ReactiveAuthorizationManager授权管理器实现动态鉴权。
3. 以后有时间再慢慢完善，如有更好的建议欢迎交流，企鹅群:238147050,验证信息请备注“gitee oauth2”。
4. 部分请求接口截图：获取token
![img.png](img.png)
5. 获取用户信息：![img_1.png](img_1.png)
6. 获取授权码：先调“http://localhost:9001/sakura-cloud-auth/oauth/authorize?client_id=sakura-app&redirect_uri=http://localhost:8848/nacos&response_type=code&scope=all”获取授权码；
![img_2.png](img_2.png)

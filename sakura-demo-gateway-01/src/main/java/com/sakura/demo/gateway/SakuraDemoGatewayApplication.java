package com.sakura.demo.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: zhengcan
 * @Date: 2022/5/13
 * @Description:
 * @Version: 1.0.0 创建
 */
@SpringBootApplication
public class SakuraDemoGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(SakuraDemoGatewayApplication.class, args);
    }
}

package com.sakura.demo.gateway.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

import javax.sql.DataSource;

/**
 * @Author: zhengcan
 * @Date: 2022/5/16
 * @Description: jwtToken存储方式配置
 * @Version: 1.0.0 创建
 */
@Configuration
public class JwtTokenStoreConfig {

    /**
     * 可选择redis或者jdbc的方式获取token，相应的在token认证服务器端要采用对应的存储方式
     */

//    @Autowired
//    private RedisConnectionFactory redisConnectionFactory;
//
//    @Bean
//    public TokenStore jwtTokenStore() {
//        return new RedisTokenStore(redisConnectionFactory);
//    }

    @Autowired
    public DataSource dataSource;

    @Bean
    public TokenStore jwtTokenStore() {
        return new JdbcTokenStore(dataSource);
    }

}

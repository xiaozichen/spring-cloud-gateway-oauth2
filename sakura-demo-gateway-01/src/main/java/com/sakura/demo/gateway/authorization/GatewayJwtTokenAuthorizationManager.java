package com.sakura.demo.gateway.authorization;

import cn.hutool.core.convert.Convert;
import com.sakura.demo.common.constant.AuthConstant;
import com.sakura.demo.common.constant.RedisConstant;
import com.sakura.demo.common.redis.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.authorization.ReactiveAuthorizationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.server.authorization.AuthorizationContext;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author: zhengcan
 * @Date: 2022/5/13
 * @Description: 鉴权管理器，用于判断是否有资源的访问权限
 * @Version: 1.0.0 创建
 */
@Slf4j
@Component
public class GatewayJwtTokenAuthorizationManager implements ReactiveAuthorizationManager<AuthorizationContext> {

    @Autowired
    private RedisService redisService;

    /**
     * 要想在Mono<Authentication>中存在Authentication信息，必须要先进行token认证操作
     *  即ReactiveAuthenticationManager认证管理器要先authenticate()后，Mono<Authentication>中才有是否认证信息
     *      调用栈：
     *      1.如有filter在AuthenticationWebFilter之前的先执行；
     *      2.带token值过来时,先进AuthenticationWebFilter.authenticate()-->调默认的JwtReactiveAuthenticationManager.authenticate();
     *      3.认证通过后根据过滤器链往下走, 若未携带token信息，在跳过AuthenticationWebFilter，直接向后执行(关于filter执行顺序请查看SecurityWebFiltersOrder);
     *      4.执行到AuthorizationWebFilter.filter()-->先验证token是否为空：
     *          a.为空则不再进行权限校验，这也是白名单的原理；
     *          b.不为空则调用DelegatingReactiveAuthorizationManager.check()-->
     *                  若存在ReactiveAuthorizationManager<AuthorizationContext>类型的实现类，再调用其check()方法验证, 即本自定义实现类；
     *                  不存在则调用默认的AuthorityReactiveAuthorizationManager.check()或其他类型实现类处理
     *
     * @param mono
     * @param context
     * @return
     */
    @Override
    public Mono<AuthorizationDecision> check(Mono<Authentication> mono, AuthorizationContext context) {
        // 从redis中获取当前路径可访问角色列表
        URI uri = context.getExchange().getRequest().getURI();
        Object obj = redisService.hGet(RedisConstant.RESOURCE_ROLES_MAP, uri.getPath());
        List<String> authorities = new ArrayList<>();
        if (null != obj) {
            // 给role的命名拼上前缀ROLE_
            authorities =
                    Convert.toList(String.class, obj)
                            .stream().map(role -> role = AuthConstant.AUTHORITY_PREFIX + role)
                            .collect(Collectors.toList());
        }
        return mono
                // token是否认证，防止绕过token漏传
                .filter(Authentication::isAuthenticated)
                // 迭代权限集合
                .flatMapIterable(Authentication::getAuthorities)
                // 取出权限值
                .map(GrantedAuthority::getAuthority)
                // 和权限集合进行匹配，任意一个匹配中
                .any(authorities::contains)
                // 则认为有访问授权
                .map(AuthorizationDecision::new)
                // 否则默认没有授权
                .defaultIfEmpty(new AuthorizationDecision(false));
    }
}

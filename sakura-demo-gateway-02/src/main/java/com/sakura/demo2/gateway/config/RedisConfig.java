package com.sakura.demo2.gateway.config;

import com.sakura.demo.common.cache.BaseRedisConfig;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: zhengcan
 * @Date: 2022/5/13
 * @Description: redis配置
 * @Version: 1.0.0 创建
 */
@Configuration
public class RedisConfig extends BaseRedisConfig {
}

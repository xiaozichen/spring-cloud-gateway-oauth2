package com.sakura.demo2.gateway.filter;


import com.sakura.demo.common.constant.AuthConstant;
import com.sakura.demo.common.constant.MessageConstant;
import com.sakura.demo2.gateway.config.IgnoreUrlsConfig;
import com.sakura.demo2.gateway.utils.WebfluxResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @Author: zhengcan
 * @Date: 2022/5/13
 * @Description: 白名单路径访问时需要移除JWT请求头
 * @Version: 1.0.0 创建
 */
@Component
public class IgnoreUrlsRemoveJwtFilter implements WebFilter {

    @Autowired
    private IgnoreUrlsConfig ignoreUrlsConfig;

    /**
     * 白名单的原理就是移除token信息，由资源管理器去处理，该filter的内容可以一并放在GatewayJwtTokenAuthorizationManager中处理
     * @param exchange
     * @param filterChain
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain filterChain) {
        ServerHttpRequest request = exchange.getRequest();
        URI uri = request.getURI();
        AntPathMatcher matcher = new AntPathMatcher();
        // 白名单路径移除JWT请求头，不进行鉴权操作
        // 不在白名单内一定要携带token进行认证
        AtomicReference<Boolean> isIgnore = new AtomicReference<>(false);
        List<String> urls = ignoreUrlsConfig.getUrls();
        if (!CollectionUtils.isEmpty(urls)) {
            urls.forEach(url -> {
                if (matcher.match(url, uri.getPath())) {
                    ServerHttpRequest newRequest = exchange.getRequest().mutate().header(AuthConstant.TOKEN_HEADER, "").build();
                    exchange.mutate().request(newRequest).build();
                    isIgnore.set(true);
                }
            });
        }
        if (!isIgnore.get()) {
            // 检验是否携带token, 防止未携带token时绕过token认证管理器
            String token = request.getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
            if (ObjectUtils.isEmpty(token)) {
                return WebfluxResponseUtil.responseFailed(exchange, HttpStatus.FORBIDDEN.value(), MessageConstant.LOSE_TOKEN);
            }
        }
        return filterChain.filter(exchange);
    }

}

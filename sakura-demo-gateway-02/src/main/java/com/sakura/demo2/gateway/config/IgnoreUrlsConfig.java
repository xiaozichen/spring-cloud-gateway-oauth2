package com.sakura.demo2.gateway.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author: zhengcan
 * @Date: 2022/5/13
 * @Description: 网关白名单配置
 * @Version: 1.0.0 创建
 */
@Getter
@Setter
@Component
@ConfigurationProperties(prefix="secure.ignore")
public class IgnoreUrlsConfig {

    private List<String> urls;

}

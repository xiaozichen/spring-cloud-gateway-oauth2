package com.sakura.demo2.gateway.config;

import com.sakura.demo2.gateway.jwt.JwtTokenEnhancer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

/**
 * @Author: zhengcan
 * @Date: 2022/5/17
 * @Description: jwt access token配置
 *                  注：要与token认证服务器端的配置保持一致
 * @Version: 1.0.0 创建
 */
@Configuration
public class JwtAccessTokenConfig {

    @Value("${jwt.secret}")
    private String jwtSecret;

    /**
     * token转换器
     * @return
     */
    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtTokenEnhancer();
        // 对称加密模式
        jwtAccessTokenConverter.setSigningKey(jwtSecret);
        return jwtAccessTokenConverter;
    }

    /**
     * 使用jwt token方式
     *  资源服务器启动时完成初始化Bean，不需要去调用认证服务获取相关算法和签名密钥
     * @return
     */
    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(jwtAccessTokenConverter());
    }

}

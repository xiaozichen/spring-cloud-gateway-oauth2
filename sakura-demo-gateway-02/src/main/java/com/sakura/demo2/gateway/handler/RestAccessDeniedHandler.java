package com.sakura.demo2.gateway.handler;

import com.sakura.demo.common.error.SystemErrorType;
import com.sakura.demo2.gateway.utils.WebfluxResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.server.authorization.ServerAccessDeniedHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @Author: zhengcan
 * @Date: 2022/5/13
 * @Description: 自定义返回结果：没有权限访问时
 * @Version: 1.0.0 创建
 */
@Slf4j
@Component
public class RestAccessDeniedHandler implements ServerAccessDeniedHandler {
    @Override
    public Mono<Void> handle(ServerWebExchange exchange, AccessDeniedException e) {
        log.error("[RestAccessDeniedHandler]：权限校验失败，e={}", e);
        return WebfluxResponseUtil.responseFailed(exchange, HttpStatus.FORBIDDEN.value(), SystemErrorType.FORBIDDEN);
    }

}

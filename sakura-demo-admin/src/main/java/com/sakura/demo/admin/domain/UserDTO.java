package com.sakura.demo.admin.domain;

import lombok.Data;

import java.util.List;

/**
 * @Author: zhengcan
 * @Date: 2022/5/13
 * @Description:
 * @Version: 1.0.0 创建
 */
@Data
public class UserDTO {
    private Long id;
    private String username;
    private String password;
    private List<String> roles;
}

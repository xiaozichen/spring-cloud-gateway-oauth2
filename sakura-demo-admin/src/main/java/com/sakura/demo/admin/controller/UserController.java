package com.sakura.demo.admin.controller;

import com.sakura.demo.admin.domain.UserDTO;
import com.sakura.demo.admin.handler.LoginUserHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: zhengcan
 * @Date: 2022/5/13
 * @Description:
 * @Version: 1.0.0 创建
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private LoginUserHolder loginUserHolder;

    @GetMapping("/currentUser")
    public UserDTO currentUser() {
        return loginUserHolder.getCurrentUser();
    }

    @GetMapping("/hello")
    public String hello() {
        return "hello";
    }

}
